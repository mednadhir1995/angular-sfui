import { Injectable } from '@angular/core';
import { HttpClient, HttpParams , HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Response} from './interface';



export class LeadModel {
/*   login: string;
  password: string; */
  FirstName: string;
  LastName: string;
  Company: string;
  Email: string;
  Website: string;
  Phone: string;

  // tslint:disable-next-line:max-line-length
  constructor( FirstName: string, LastName: string, Company: string, Email: string, Website: string, Phone: string) {
  /*   this.login = login;
    this.password = password */;
    this.FirstName = FirstName;
    this.LastName = LastName;
    this.Company = Company;
    this.Email = Email;
    this.Website = Website;
    this.Phone = Phone;
  }
}







@Injectable({
  providedIn: 'root'
})
export class AppSerService {

  constructor(private  httpClient: HttpClient) { }

  readonly host = 'http://localhost:5000/ocr/elastic/name';
  readonly host3 = 'http://localhost:5000/ocr/elastic/lastname';

  readonly host2 = 'http://localhost:9200/names/_doc/';
  readonly host4 = 'http://localhost:9200/lastnames/_doc/';
  readonly host5 = 'http://localhost:9200/companynames/_doc/';
  readonly host6 = 'http://localhost:5000/ocr/elastic/company';

  readonly testurl = 'http://localhost:5000/ocr/add';
  public autoComplete(name: string): Observable<Response> {


    const params = new HttpParams()
        .set('name', name);
    return this.httpClient.get<Response>(this.host, { params});
}


public autoCompleteLastname(lastname: string): Observable<Response> {


  const params = new HttpParams()
      .set('lastname', lastname);
  return this.httpClient.get<Response>(this.host3, { params});
}

public autoCompleteCompanyname(company: string): Observable<Response> {


  const params = new HttpParams()
      .set('company', company);
  return this.httpClient.get<Response>(this.host6, { params});
}



addName(firstname: string): Observable<any> {
  /* let headers = new HttpHeaders()
  .set('Access-Control-Allow-Origin', '*')
  .set('Access-Control-Allow-Credentials', 'true')
  .set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
  // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:max-line-length
  .set('Access-Control-Allow-Headers',
  'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With,
   Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'); */

  return this.httpClient.post(this.host2, {firstname});
}

addLastname(lastname: string): Observable<any> {

  return this.httpClient.post(this.host4, {lastname});
}


/*
log(username: string, password: string): Observable<any> {
  const params = new HttpParams()
  .set('grant_type', 'password')
  .set('username', username)
  .set('password', password)
  .set('client_id', '3MVG9I9urWjeUW06H8FTkYR.HNfGLgCpClaJCTdJdw9ZLxBIprh.IwJ2R_wqGLSCCvluAGm3qnDMuGHocUWod')
  .set('client_secret', '6743D11CC5C806D0BE168A8A138D0C2477CDF874D06351F4C00C6276C7CCF8B4');
  return this.httpClient.post(this.authHost, { params}) ;
} */





sendDatatosalesforce(f: string, f2: string, lead: LeadModel ): Observable<any> {
  const  body = {f, f2, lead};
  return this.httpClient.post(this.testurl, body) ;

}


}




